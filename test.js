/**
 * Created by blackSheep on 12-Jul-17.
 */
var KalmanFilter = require('kalmanjs').default;

var kf = new KalmanFilter();
kf.filter(2);
var myArray = [0,0,1,5,2,8,68,0,2,2];
//var kalmanFilter = new KalmanFilter({R: 0.01, Q: 6.8});
//var kalmanFilter = new KalmanFilter({R: 0.1, Q: 20});
var kalmanFilter = new KalmanFilter({R: 1, Q: 40});
var dataConstantKalman = myArray.map(function(v) {
    return kalmanFilter.filter(v);
});
console.log(dataConstantKalman);